# My Tennis Club

This project is based on the tutorial at W3Schools.
It creates a basic CRUD Front End and Backend application based on the principles of MVT (Model, View, Template by Python)
Tutorial Url: https://www.w3schools.com/django/django_create_project.php

## Table of Contents

- [My Tennis Club](#My-Tennis-Club)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Features](#features)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Configuration](#configuration)
  - [Contributing](#contributing)
  - [License](#license)

## Introduction

Provide a brief introduction to your project. Explain its purpose and any key features.

## Features

List the main features of your project.

- Feature 1
- Feature 2
- Feature 3

## Installation - Dev (Docker)

1. Go to the root directory of the app and run `docker-compose up`
2. Once the server is running on docker navigate to `http://127.0.0.1:8000/`

## Installation - Dev (Local)

Provide instructions on how to install your project. Include any prerequisites and steps necessary to set up the environment.

1. Istall Python 3:
   If you don't have Python 3 installed or you want to install a newer version, you can download the latest version of Python from the official website: https://www.python.org/downloads/
   Download the macOS installer for the latest Python 3.x version.
   Open the downloaded file and follow the installation instructions.
   Make sure to check the box that says "Add Python 3.x to PATH" during installation. This will make Python accessible from the Terminal.

2. Install django:
   `python -m pip install Django`

3. Start python server:
   `py manage.py runserver`

4. Seed demo data:
   `python3 manage.py seed_data`

5. Undo seeders:
   `python3 manage.py migrate members zero`

## Usage

Provide examples and instructions on how to use your project. Include code snippets or screenshots if helpful.

```bash
# Example usage command
$ python manage.py runserver
```
