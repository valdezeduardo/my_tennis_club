from django.db import models
class Member(models.Model):
 firstName = models.CharField(max_length=255)
 lastName = models.CharField(max_length=255)
 email = models.EmailField(max_length=255, null=True, blank=True)
 birthDay = models.DateField(null=True, blank=True)
