
from django.core.management.base import BaseCommand
from members.models import Member

class Command(BaseCommand):
    help = 'Seed the database with initial data'

    def handle(self, *args, **kwargs):
        seed_data = [
            {'firstName': 'Joe', 'lastName': 'Smith', 'email' : 'joe.smith@mail.com', 'birthDay' : '1980-01-1'},
            {'firstName': 'Tom', 'lastName': 'Hanks', 'email' : 'tom.hanks@mail.com', 'birthDay' : '1980-02-1'},
            {'firstName': 'Tobias', 'lastName': 'Refsnes', 'email' : 'tobias.refsnes@mail.com', 'birthDay' : '1980-03-03'},
            {'firstName': 'Linus', 'lastName': 'Refsnes', 'email' : 'linus.refsnes@mail.com', 'birthDay' : '1980-04-04'},
            {'firstName': 'Lene', 'lastName': 'Refsnes', 'email' : 'lene.refsnes@mail.com', 'birthDay' : '1980-05-05'},
            {'firstName': 'Stale', 'lastName': 'Refsnes', 'email' : 'stale.refsnes@mail.com', 'birthDay' : '1980-06-06'},
            {'firstName': 'Jane', 'lastName': 'Doe', 'email' : 'jane.doe@mail.com', 'birthDay' : '1980-06-06'},   
        ]

        for data in seed_data:
            Member.objects.create(**data)

        self.stdout.write(self.style.SUCCESS('Seed data inserted successfully'))
